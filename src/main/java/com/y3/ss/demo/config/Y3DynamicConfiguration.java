package com.y3.ss.demo.config;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;

import com.y3.ss.demo.model.ConfigURL;
import com.y3.ss.demo.model.Role;
import com.y3.ss.demo.repo.ConfigURLRepo;

@Component
public class Y3DynamicConfiguration implements FilterInvocationSecurityMetadataSource {


  @Autowired
  private ConfigURLRepo configUrls;

  @Override
  public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
    final HttpServletRequest request = ((FilterInvocation) object).getRequest();

    String requestUrl = request.getRequestURI().substring(request.getContextPath().length());

    Optional<ConfigURL> allowedUrl = configUrls.findByUrl(requestUrl);

    if (allowedUrl.isPresent()) {
      System.out.println("Permitted URL" + requestUrl);
      return allowedUrl.get().getRolesAllowed().stream().map(this::configAttribute)
          .collect(Collectors.toList());
    }

    return null;
  }

  private ConfigAttribute configAttribute(Role role) {
    return new ConfigAttribute() {
      /**
      * 
      */
      private static final long serialVersionUID = -1027466909938369199L;

      @Override
      public String getAttribute() {
        return "ROLE_USER";
      }
    };
  }

  @Override
  public Collection<ConfigAttribute> getAllConfigAttributes() {
    return null;
  }

  @Override
  public boolean supports(Class<?> clazz) {
    return FilterInvocation.class.isAssignableFrom(clazz);
  }



}


