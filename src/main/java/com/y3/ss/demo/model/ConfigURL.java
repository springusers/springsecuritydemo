package com.y3.ss.demo.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;



@Entity
public class ConfigURL {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column(unique = true)
  private String url;
  // @OneToMany
  @Transient
  private List<Role> rolesAllowed;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public List<Role> getRolesAllowed() {
    return Arrays.asList("ROLE_USER").stream().map(e -> new Role(e)).collect(Collectors.toList());
  }

  public void setRolesAllowed(List<Role> rolesAllowed) {
    this.rolesAllowed = rolesAllowed;
  }

  public ConfigURL() {

  }

  public ConfigURL(String path, String role) {
    super();
    this.url = path;
    this.setRolesAllowed(
        Arrays.asList(role.split(",")).stream().map(e -> new Role(e)).collect(Collectors.toList()));
  }

}
