package com.y3.ss.demo.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.y3.ss.demo.model.ConfigURL;

@Repository
public interface ConfigURLRepo extends JpaRepository<ConfigURL, Long> {

  public Optional<ConfigURL> findByUrl(@Param("url") String url);
}
