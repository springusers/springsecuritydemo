package com.y3.ss.demo;

import java.util.Optional;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.y3.ss.demo.config.Y3DynamicConfiguration;
import com.y3.ss.demo.model.ConfigURL;
import com.y3.ss.demo.repo.ConfigURLRepo;

@SpringBootApplication
public class SpringSecurityDemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringSecurityDemoApplication.class, args);
  }



}


@Entity
class Account {

  @Id
  @GeneratedValue
  private long id;
  private String username, password, authorities;

  protected Account() {} // mandates by JPA

  public Account(String username, String password, String authorities) {
    this.username = username;
    this.password = password;
    this.authorities = authorities;
  }

  public long getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getAuthorities() {
    return authorities;
  }
}


@Repository
interface AccountRepository extends JpaRepository<Account, Long> {

  Optional<Account> findByUsername(String username);
}


// actual configuration
@Configuration
class SecurityConfig {


  @Bean
  UserDetailsService userDetailsService(AccountRepository accountRepository) {
    return username -> accountRepository.findByUsername(username)
        .map(a -> new User(a.getUsername(), a.getPassword(),
            AuthorityUtils.commaSeparatedStringToAuthorityList(a.getAuthorities())))
        .orElseThrow(() -> new UsernameNotFoundException(username));
  }

  @Bean
  ApplicationRunner initUsers(PasswordEncoder encoder, AccountRepository repository) {
    // init db, for demo
    return args -> Stream
        .of(new Account("user", encoder.encode("password"), "ROLE_USER"),
            new Account("admin", encoder.encode("password"), "ROLE_USER,ROLE_ADMIN"))
        .forEach(repository::save);
  }

  @Bean
  ApplicationRunner initSystemUrl(ConfigURLRepo repository) {
    // init db, for demo
    return args -> Stream.of(new ConfigURL("/b", "USER"), new ConfigURL("/b/1", "ROLE_ADMIN"))
        .forEach(repository::save);
  }

}


// simple jpa entity mapping
@Entity
class Book {

  @Id
  @GeneratedValue
  long id;

  private String title;
  private int year;

  protected Book() {} // mandates by JPA

  public Book(String title, int year) {
    this.title = title;
    this.year = year;
  }

  public long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public int getYear() {
    return year;
  }

  @Override
  public String toString() {
    return "Book{id=" + id + ", title='" + title + '\'' + ", year=" + year + '}';
  }
}


@Repository
interface BookRepository extends JpaRepository<Book, Long> {
}


@RestController
class BookController {

  @Autowired
  BookRepository repository;

  @RequestMapping("/b/{id}")
  Book getById(@PathVariable long id) {
    return repository.findOne(id);
  }
}


@Configuration
class BookConfig {

  @Bean
  ApplicationRunner init(BookRepository repository) {
    // init db
    return args -> {
      repository.save(new Book("Java tutorial", 1995));
      repository.save(new Book("Spring reference", 2016));
    };
  }
}


@Component
@EnableWebSecurity
class Y3SecConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    AffirmativeBased affirmativeBased =
        new AffirmativeBased(java.util.Arrays.asList(new RoleVoter(), new WebExpressionVoter()));
    http.authorizeRequests().accessDecisionManager(affirmativeBased).antMatchers("/login")
        .permitAll().antMatchers(HttpMethod.GET, "/login/logout").permitAll()
        .antMatchers(HttpMethod.GET, "/logout").permitAll().and().csrf().disable().formLogin().and()
        .authorizeRequests().anyRequest().authenticated()
        .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
          public <O extends FilterSecurityInterceptor> O postProcess(O fsi) {
            fsi.setSecurityMetadataSource(y3DynamicConfiguration);
            return fsi;
          }
        }).and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/login/logout"))
        .permitAll();
    // .and().logout().logoutSuccessUrl("/login/logout").permitAll().and().csrf().disable();

  }



  @Autowired
  private UserDetailsService users;
  @Autowired
  private Y3DynamicConfiguration y3DynamicConfiguration;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(users).passwordEncoder(new BCryptPasswordEncoder());
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/resources/**");
  }

  @Bean
  static PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

}


